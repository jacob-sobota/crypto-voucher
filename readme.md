## Build Setup

``` bash
# install dependencies
$ npm install --unsafe-perm

# update CSS
$ gulp sass

# update CSS reload
$ gulp sass:watch
```
